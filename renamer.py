#!/usr/bin/env python3
import os
import click


def rename(root: str, fname: str, func: 'function') -> None:
    try:
        os.rename(
            os.path.join(root, fname), os.path.join(root, func(fname))
        )
    except FileNotFoundError as e:
        print(e)


def rename_files_and_dirs(path: str, func: 'function') -> None:
    for root, dirs, files in os.walk(path, topdown=False):
        for file_name in files:
            rename(root, file_name, func)
        for dir_name in dirs:
            rename(root, dir_name, func)


@click.command()
@click.option('--upper', '-u', is_flag=True)
@click.argument('path')
def cli(path, upper):
    func = (lambda x: x.lower()) if not upper else (lambda x: x.upper())
    rename_files_and_dirs(path, func)


if __name__ == '__main__':
    cli()
