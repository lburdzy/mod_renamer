import os
import shutil
import click


def move(src_path: str, dst_path: str, rewrite: bool):
    try:
        shutil.move(src_path, dst_path)
    except FileExistsError as e:
        if not rewrite:
            raise e
        else:
            print(e.message)


@click.command()
@click.option('--rewrite', '-r', is_flag=True)
@click.argument('src_path')
@click.argument('dst_path')
def cli(src_path: str, dst_path: str, rewrite: bool) -> None:
    for d in next(os.walk(src_path))[1]:
        move(os.path.join(src_path, d), dst_path, rewrite)


if __name__ == '__main__':
    cli()
